from math import log2

class Letter:
    """
    Класс для хранения информации о букве

    name - название буквы

    freq - частота

    poss - вероятность
    """
    def __init__(self, name, freq=0, poss=0.0):
        self.name = name
        self.freq = freq
        self.poss = poss
        self.code = ""


    def code_reset(self):
        """
        Сброс кода буквы
        """
        self.code = ""


    def code_lenght(self):
        """
        Длина кода одной буквы
        """
        return len(self.code)

    
    def entropy_elmt(self):
        """
        Составляющая энтропии
        """
        return -self.poss * log2(self.poss)

    
    def n_average_elmt(self):
        """
        Определение слагаемого для подсчета средней длины кода
        """
        return self.poss * self.code_lenght()

class ShannonCode:
    """
    Класс для построения кода Шеннона
    """
    def __init__(self, letters):
        """
        Конструктор принимает на вход список букв 
        """
        self.letters = letters


    def sort(self):
        """
        Упорядочивает буквы по убыванию частот
        """
        self.letters = sorted(
            self.letters, key=lambda elmt: elmt.poss, reverse=True)

        if self.letters[len(self.letters) - 1].poss == 0.0:
            # удялем из рассмотрения символы, которые ни разу не встретились
            i = 0  # счетчик нулевых букв

            for elmt in self.letters:
                if elmt.poss == 0.0:
                    break
                i += 1

            self.letters = self.letters[:i]

        return self.letters


    def poss_sum(self, l):
        """
        Поиск суммы част

        l - список букв
        """
        res = 0.0
        for i in range(len(l)):
            res += l[i].poss

        return res


    def create_code(self, working_list):
        """
        Создает коды для букв

        working_list - часть от общего списка букв
        """
        # if len(working_list) == 2:
        #     working_list[0].code += "0"
        #     working_list[1].code += "1"
        #     return

        if len(working_list) < 2:
            return

        working_list[0].code += "0"
        left = [working_list[0]]   # левая половина массива
        right = []  # правая половина массива
        cutted = False  # флаг перехода к следующей половине

        border = self.poss_sum(working_list) / 2
        
        for i in range(1, len(working_list)):
            #cur_sum = 
            if (self.poss_sum(left) + working_list[i].poss < 1.1 * border) and (not cutted):
                working_list[i].code += "0"
                left.append(working_list[i])
            
            else:
                cutted = True
                working_list[i].code += "1"
                right.append(working_list[i])

        # рекурсивные вызовы
        self.create_code(left)
        self.create_code(right)


        
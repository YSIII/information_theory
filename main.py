#TODO 
'''
[+] написать функцию по сохранению результатов в файл
[] найти тексты на 10 000 символов (научный и художественный)
[] подготовить отчет
'''

from shannon_code import ShannonCode
from letter import *
from block import Block

# алфавит
__ALPHABET = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж',
              'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о',
              'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц',
              'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']


def file_reader(path, mode):
    """
    Считывает и возвращает текст из файла

    path - путь к фалу

    mode - режим работы с файлом
    """
    text_file = open(path, mode, encoding="UTF-8")
    text = text_file.read()
    text_file.close()

    return text


def save_results(path, mode, letters):
    """
    Сохранение информации о букве в файл

    letters - список букв
    """
    log_file = open(path, mode, encoding="UTF-8")

    for elmt in letters:
        str_to_save = "имя: {0} | частота: {1:5} | вероятность: {2:.10f} | код : {3} \n".format(
            elmt.name, elmt.freq, elmt.poss, elmt.code)
        
        log_file.writelines(str_to_save)

    log_file.writelines("=" * 90 + "\n")

    log_file.close()


def get_info(letters):
    """
    Вывод информации о результатх кодирования

    l - список букв
    """
    print("=" * 30 + "\n"
        "Энтропия кода = {0}".format(entropy(letters)))

    print("Средняя длина кода = {0}".format(n_average(letters)))

    print("Избыточность кода = {0}".format(code_redundancy(letters)))
    
    print("Эффективность кода = {0}\n".format(code_efficiency(letters)) +
        "=" * 30)


def sum_counter(text, letters):
    """
    Подсчет всех алфавитных символов

    text - текст

    letters - список букв
    """
    answr = 0

    for symbol in text:
        if symbol in __ALPHABET:
            for letter in letters:
                if letter.name == symbol:
                    letter.freq += 1
            
            answr += 1

    return answr


def possibility_counter(letters, sum):
    """
    Определение вероятности каждой буквы

    sum - общее кол-во букв

    letters - список букв
    """
    for elmt in letters:
        elmt.poss = elmt.freq / sum


def entropy(letters):
    """
    Определение энтропии кода

    letters - список букв
    """
    answr = 0.0

    for elmt in letters:
        answr += elmt.entropy_elmt()

    return answr


def n_average(letters):
    """
    Определение среденей длинны кода

    letters - список букв
    """
    answr = 0.0

    for elmt in letters:
        answr += elmt.n_average_elmt()

    return answr


def code_redundancy(letters):
    """
    Избыточность кода

    letters - список букв
    """
    return 1 - (entropy(letters) / n_average(letters))


def code_efficiency(letters):
    """
    Эффективность кода

    letters - спсиок букв
    """
    return entropy(letters) / n_average(letters)


def main():
    """
    Основная программа
    """
    # список букв
    letters = []

    # заполнение списка
    for symbol in __ALPHABET:
        letters.append(Letter(symbol))

    # перевести буквы в строчной регистр
    text = file_reader("report.txt", "r").lower()
    
    # кол-во алфавитных символов
    total = sum_counter(text, letters)
    print(total)

    # определение вероятности для каждой буквы
    possibility_counter(letters, total)

    """--------------------------одиночное кодирование-----------------------------"""
    # построение кода
    solution = ShannonCode(letters)
    sorted_letters = solution.sort()
    solution.create_code(sorted_letters)

    get_info(sorted_letters)
    save_results("log.txt", "w", sorted_letters)

    """-----------------------------блок из 2х букв---------------------------------"""
    block2 = Block.create_2Lblock(letters)
    solution2 = ShannonCode(block2)
    sorted_block2 = solution2.sort()
    solution2.create_code(sorted_block2)

    get_info(sorted_block2)
    save_results("log.txt", "a", sorted_block2)

    """-----------------------------блок из 3х букв---------------------------------"""
    block3 = Block.create_3Lblock(letters)
    solution3 = ShannonCode(block3)
    sorted_block3 = solution3.sort()
    solution2.create_code(sorted_block3)

    get_info(sorted_block3)
    save_results("log.txt", "a", sorted_block3)


# вызов основной функции
if __name__ == "__main__":
    main()


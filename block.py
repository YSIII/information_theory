from letter import Letter

class Block:
    """
    Содержит функции по группировке букв
    """
    def create_2Lblock(letters):
        """
        Создание блоков из 2х символов

        letters - список букв
        """
        block = []

        for i in range(len(letters)):
            for j in range(len(letters)):
                block.append(Letter(letters[i].name + letters[j].name, 0, letters[i].poss * letters[j].poss))

        return block


    def create_3Lblock(letters):
        """
        Создание блоков из 3ч символов

        letters - список букв
        """
        block = []
        tmp = Block.create_2Lblock(letters)

        for i in range(len(tmp)):
            for j in range(len(letters)):
                block.append(Letter(tmp[i].name + letters[j].name, 0, tmp[i].poss * letters[j].poss))

        return block


